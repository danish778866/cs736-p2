#!/bin/bash -x

SCRIPT_DIR=$(cd `dirname $0` && pwd)

# Install postgresql
sudo apt-get -qq install postgresql

# Create tpch_user and tpch database
sudo su -c "psql -p 5432 postgres -c \"CREATE ROLE tpch_user PASSWORD '1234' CREATEDB CREATEROLE SUPERUSER LOGIN;\"" postgres
sudo su -c "createdb -O tpch_user tpch" postgres

# Add pgpass file to home directory in order to enable passwordless authentication of tpch_user
pushd ~
echo "*:5432:tpch:tpch_user:1234" > ".pgpass"
chmod 0600 ".pgpass"
popd

# Extract the tpch-postgres tar file
pushd $SCRIPT_DIR
tar -xzf tpch-postgres.tgz
popd

# Execute the schema.sql file to build the schema and load tpch data
pushd ${SCRIPT_DIR}
time psql -p 5432 -U tpch_user -h localhost tpch -f schema.sql
popd
