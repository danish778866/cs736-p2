#!/bin/bash

# Set Script Name variable
SCRIPT=`basename ${BASH_SOURCE[0]}`

# Initialize variables to default values.
M="50G"
C=20
D="500G"
N=1000000
P=20
X=100000
V=1

# Set fonts for Help.
NORM=`tput sgr0`
BOLD=`tput bold`
REV=`tput smso`

# Help function
function HELP {
  echo -e \\n"Help documentation for ${BOLD}${SCRIPT}.${NORM}"\\n
  echo -e "${REV}Basic usage:${NORM} ${BOLD}$SCRIPT file.ext${NORM}"\\n
  echo "Command line switches are optional. The following switches are recognized."
  echo "${REV}-m${NORM}  --The amount of memory allocated to the VM ${BOLD}m${NORM}. Default is ${BOLD}50G${NORM}."
  echo "${REV}-c${NORM}  --The number of cores allocated to the VM ${BOLD}c${NORM}. Default is ${BOLD}20${NORM}."
  echo "${REV}-d${NORM}  --The amount of disk space allocated to the VM ${BOLD}d${NORM}. Default is ${BOLD}500G${NORM}."
  echo "${REV}-n${NORM}  --Sets the total number of TPC-H Q1 queries to be executed ${BOLD}n${NORM}. Default is ${BOLD}1000000000${NORM}."
  echo "${REV}-p${NORM}  --Sets the number of threads to execute the queries from ${BOLD}p${NORM}. Default is ${BOLD}20${NORM}."
  echo "${REV}-x${NORM}  --Sets the number of TPC-H Q1 queries to be executed per thread ${BOLD}x${NORM}. Default is ${BOLD}100000${NORM}."
  echo "${REV}-v${NORM}  --Sets the number of VMs executing the benchmarks ${BOLD}v${NORM}. Default is ${BOLD}1${NORM}."
  echo -e "${REV}-h${NORM}  --Displays this help message. No further functions are performed."\\n
  echo -e "Example: ${BOLD}$SCRIPT -m 50G -c 20 -d 500G -n 1000000 -p 20 -x 100000${NORM}"\\n
  exit 1
}

# Check the number of arguments. If none are passed, print help and exit.
NUMARGS=$#
echo -e \\n"Number of arguments: $NUMARGS"
if [ $NUMARGS -eq 0 ]; then
  HELP
fi

### Start getopts code ###

#Parse command line flags
#If an option should be followed by an argument, it should be followed by a ":".
#Notice there is no ":" after "h". The leading ":" suppresses error messages from
#getopts. This is required to get my unrecognized option code to work.

while getopts :m:c:d:n:p:x:v:h FLAG; do
  case $FLAG in
    m)  #set option "m"
      M=$OPTARG
      echo "-m used: $OPTARG"
      echo "M = $M"
      ;;
    c)  #set option "c"
      C=$OPTARG
      echo "-c used: $OPTARG"
      echo "C = $C"
      ;;
    d)  #set option "d"
      D=$OPTARG
      echo "-d used: $OPTARG"
      echo "D = $D"
      ;;
    n)  #set option "n"
      N=$OPTARG
      echo "-n used: $OPTARG"
      echo "N = $N"
      ;;
    p)  #set option "p"
      P=$OPTARG
      echo "-p used: $OPTARG"
      echo "P = $P"
      ;;
    x)  #set option "x"
      X=$OPTARG
      echo "-x used: $OPTARG"
      echo "X = $X"
      ;;
    v)  #set option "v"
      V=$OPTARG
      echo "-v used: $OPTARG"
      echo "V = $V"
      ;;
    h)  #show help
      HELP
      ;;
    \?) #unrecognized option - show help
      echo -e \\n"Option -${BOLD}$OPTARG${NORM} not allowed."
      HELP
      ;;
  esac
done

shift $((OPTIND-1))  #This tells getopts to move on to the next argument.

### End getopts code ###

BENCHMARK_DIR=$(cd `dirname $0` && pwd)
DATA_FILE="`hostname`_data_M${M}_C${C}_D${D}_V${V}.txt"
LOG_FILE="`hostname`_data_M${M}_C${C}_D${D}_V${V}.log"
pushd $BENCHMARK_DIR
(seq $N | xargs -n $X -P $P -I NONE ./tpch-exec.sh) >> $DATA_FILE
cat ${DATA_FILE} | grep "Time:" | cut -d " " -f2 > ${LOG_FILE}
rm -vf ${DATA_FILE}
popd
