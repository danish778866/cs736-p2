import xml.etree.ElementTree as ET

data = {}
scenario_num = 0
scenarios = ET.parse('data/experiment_data.xml').getroot()
for scenario in scenarios:
    data[scenario_num] = {}
    for metric in scenario:
        data[scenario_num][metric.tag] = []
        for server in metric:
            if server.tag != "command":
                data[scenario_num][metric.tag].append(server.text.split(","))
    scenario_num = scenario_num + 1

print(data)
