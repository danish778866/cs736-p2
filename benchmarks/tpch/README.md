# TPC-H Benchmarks execution suite
## Benchmark setup
1. Start 4 guests with M,C and D configured according to the scenario listed below.
2. Execute the script _tpch-setup.sh_ on each guest.
3. Execute the script _tpch-exec.sh_ for 1million times on each guest and record the execution times.
## Benchmark Environment
### Host
- Memory : 158GB
- CPUs   : 40
- Disk   : 1.1TB
### VMs
1. Constants
    - Number of VMs : 4
2. Variables
    - Memory (M)
    - CPUs (C)
    - Disk (D)
### TPC-H
- Scale factor : 1
- Query        : Q1
## Benchmark Scenarios
1. M=8GB, C=1, D=32GB
2. M=50GB, C=20, D=500GB
3. M=100GB, C=40, D=1000GB

## Data to be collected for each scenario
1. Average execution time of Q1 when benchmark is running on one VM.
2. Average execution time of Q1 when benchmark is running on all VMs.
3. Average execution time of Q1 when benchmark is running on 3 VMs and one VM is malicious.

## Graphs to be plotted
1. Execution time histogram of data collected in 1,2,3 above for all benchmark scenarios.
2. Grafana load/memory statistics if they're interesting.
