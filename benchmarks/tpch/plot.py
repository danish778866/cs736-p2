#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.style as style
import xml.etree.ElementTree as ET
import sys, os

def parse_xml_data(xml_file):
    data = {}
    scenario_num = 0
    scenarios = ET.parse('data/experiment_data.xml').getroot()
    for scenario in scenarios:
        data[scenario_num] = {}
        for metric in scenario:
            data[scenario_num][metric.tag] = []
            for server in metric:
                if server.tag != "command":
                    temp_list = server.text.split(",")
                    data_list = []
                    for i in temp_list:
                        data_list.append(float(i))
                    data[scenario_num][metric.tag].append(data_list)
        scenario_num = scenario_num + 1
    return data

def subcategorybar(X, vals, labels, hatches, width=0.8, colors=None):
    n = len(vals)
    _X = np.arange(len(X))
    for i in range(n):
        plt.bar(_X - width/2. + i/float(n)*width, vals[i],
                width=width/float(n), align="edge", label=labels[i], hatch = hatches[i], color=colors[i], edgecolor="black")
    plt.xticks(_X, X)

def plot_graph(scenario, metric_plot, figure_path, data):
    plt.set_cmap('copper')
    style.use('seaborn-poster') #sets the size of the charts
    style.use('ggplot')
    plt.figure(1, figsize=(5, 3.5))
    plt.rcParams.update({'font.size': 14})
    plt.rc('legend',fontsize=12)
    plt.rc('legend',loc='upper left')
    hatches = ('/', '\\', 'x', 'o', '|', '*', '+', 'O', '-', '.')
    colors = ('#dbcccc', '#8faab3', '#2e8982', '#54bad0', '#255a96', '#7f93b6', '#626e9a', '#4f5283', '#2e2c56', '#221d36') 
    if metric_plot == "load":
        labels = ['Host', 'VM1', 'VM2', 'VM3', 'VM4']
        xticks = ('s1_n1', 's1_n2', 's1_n3', 's1_n4', 's2_n4')
        print(data[scenario][metric_plot])
        subcategorybar(xticks, data[scenario][metric_plot], labels, hatches, colors=colors)
        plt.title('Avg. load on hosts and guests\nfor scenarios s1 and s2')
        plt.ylim(0, 80)
        plt.ylabel("Average long term\nload on the server")
        plt.xlabel("Scenario(s) and number of\nactive guests(n)")
    elif metric_plot == "cpu_steal":
        labels = ['VM1', 'VM2', 'VM3', 'VM4']
        xticks = ('s1_n1', 's1_n2', 's1_n3', 's1_n4', 's2_n4')
        subcategorybar(xticks, data[scenario][metric_plot], labels, hatches, colors=colors)
        plt.title('Avg. CPU Steal on guests\nfor scenarios s1 and s2')
        plt.ylim(0, 50)
        plt.ylabel("Average CPU steal across\nall guest cores")
        plt.xlabel("Scenario(s) and number(n)\nof active guests")
    elif metric_plot == "execution_time":
        labels = ['VM1', 'VM2', 'VM3', 'VM4']
        xticks = ('s1_n1', 's1_n2', 's1_n3', 's1_n4', 's2_n4')
        subcategorybar(xticks, data[scenario][metric_plot], labels, hatches, colors=colors)
        plt.title('Avg. TPC-H Q1 execution time on\nguests for scenarios s1 and s2')
        plt.ylim(0, 200)
        plt.ylabel("Average TPC-H Q1\nexecution time (s)")
        plt.xlabel("Scenario(s) and number(n)\nof active guests")
    
    plt.legend()
    plt.tight_layout()
    plt.savefig(figure_path, bbox_inches="tight")
    plt.show()

if __name__ == "__main__":
    metric_plot = sys.argv[1]
    scenario = int(sys.argv[2])
    figure_path = "figures" + os.sep + metric_plot + "_s" + str(scenario) + ".png"
    xml_file = "data/experiment_data.xml"
    data = parse_xml_data(xml_file)
    plot_graph(scenario, metric_plot, figure_path, data)
