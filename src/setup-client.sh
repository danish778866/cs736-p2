#! /usr/bin/env bash
goToSleep() {
    # stop the VM, requesting stops until it's down.
    tries=0
    STATUS=""
    while [[ "$STATUS" != "shut off" && "$tries" < 6 ]]
    do
      ((tries++))
        sudo virsh shutdown $1
        sleep 10
        STATUS="$(sudo virsh domstate $1)"
        echo "$1: shutting down: ${STATUS} ($tries/6)"
    done
    echo "Sleeping to make bloody-well sure."
    sleep 10
}

wakeUp() {
    # start the VM, requesting starts until it's up.
    tries=0
    STATUS=""
    while [[ "$STATUS" != "running" && "$tries" < 6 ]]
    do
      ((tries++))
        sudo virsh start $1
        sleep 10
        STATUS="$(sudo virsh domstate $1)"
        echo "$1: starting up: ${STATUS} ($tries/6)"
    done
    echo "Sleeping to make bloody-well sure."
    sleep 10
}

# create vm and append ip to /etc/hosts.
pushd contrib/kvm-install-vm
sudo ./kvm-install-vm create -m 8192 -t ubuntu1804 $1 | tee $1.log
sync; sleep 5
awk '/SSH to/ { sub("ubuntu@","",$6); sub("ubuntu@","",$9); printf("%s\t%s\n",$6, $9)}' $1.log \
    | tr -d "'" \
    | sudo tee -a /etc/hosts
popd

# shutdown before modifying the disk
goToSleep $1

# start the guest
sudo qemu-img resize ~/virt/vms/$1/$1.qcow2 32G
wakeUp $1

# install this project onto the guest.
echo "$1: Installing project..."
ssh ubuntu@$1 git clone https://gitlab.com/nickdaly/cs736-p2

# fix disk sizes in the guest
echo "$1: Resizing disk..."
ssh ubuntu@$1 sudo cs736-p2/src/fix-gpt.sh
goToSleep $1
wakeUp $1
ssh ubuntu@$1 sudo resize2fs /dev/vda1

# Append monitoring server resolution to the VM in order to make collectd resolve the monitoring server
MONITOR_SERVER=`cat /etc/hosts | grep node-1`
ssh ubuntu@$1 "sudo su -c \"echo ${MONITOR_SERVER} >> /etc/hosts\""

# Install and configure collectd on guest
SCRIPT_DIR="/home/ubuntu/cs736-p2/src/monitoring"
ssh ubuntu@$1 "${SCRIPT_DIR}/configure-client.sh"
