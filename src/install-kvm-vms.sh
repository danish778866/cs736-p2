#! /usr/bin/env bash
# Fetch updates and install necessary packages
sudo apt-get update -qq
sudo apt-get install -qq qemu-kvm libvirt-clients libvirt-daemon-system virt-manager

# Add yourself to the necessary groups
sudo adduser `whoami` libvirt
sudo adduser `whoami` libvirt-qemu

# Generate the default ssh key for connecting to guests
# Just leave defaults for the generation
ssh-keygen -q -t rsa -N "" -f ~/.ssh/id_rsa

# Check out the setup script for creating the vm guests
mkdir -p contrib
git clone https://github.com/wpgallih/kvm-install-vm.git contrib/kvm-install-vm
