"""This profile consists of two linked nodes, node-0 and node-1. Monitoring for node-0 and VMs running on node-0 are configured using Grafana, Graphite and Collectd on node-1."""

#
# NOTE: This code was machine converted. An actual human would not
#       write code like this!
#

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a portal object,
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Node node-0
node_0 = request.RawPC('node-0')
node_0.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD'
node_0.addService(pg.Install('https://gitlab.com/nickdaly/cs736-p2/-/archive/master/cs736-p2-master.tar.gz','/local'))
node_0.addService(pg.Execute(shell="bash", command="/local/cs736-p2-master/src/monitoring/configure-client.sh"))
iface0 = node_0.addInterface('interface-1')

# Node node-1
node_1 = request.RawPC('node-1')
node_1.disk_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD'
node_1.addService(pg.Install('https://gitlab.com/nickdaly/cs736-p2/-/archive/master/cs736-p2-master.tar.gz','/local'))
node_1.addService(pg.Execute(shell="bash", command="/local/cs736-p2-master/src/monitoring/configure-server.sh"))
iface1 = node_1.addInterface('interface-0')

# Link link-0
link_0 = request.Link('link-0')
link_0.Site('undefined')
link_0.addInterface(iface1)
link_0.addInterface(iface0)


# Print the generated rspec
pc.printRequestRSpec(request)
