(require 'cl)
(defun add-tangled-name (backend)
  (let ((src-blocks (org-element-map (org-element-parse-buffer) 'src-block #'identity)))
    (setq src-blocks (nreverse src-blocks))
    (loop for src in src-blocks
          do
          (goto-char (org-element-property :begin src))
          (let ((tangled-name (org-element-property :name src)))
            (if (bound-and-true-p tangled-name)
                (insert (format "/<%s>/ =\n" tangled-name)))))))

(add-hook 'org-export-before-parsing-hook 'add-tangled-name)

;; (defun add-references (backend)
;;   (let*
;;       ((src-blocks
;;         (org-element-map
;;             (org-element-parse-buffer) 'src-block
;;           (lambda (src)
;;             (when (org-element-property :name src)
;;               src))))
;;        (refs
;;         (cl-loop
;;          for src in src-blocks collect
;;          (let* ((name (org-element-property :name src))
;;                 (references
;;                  (string-join
;;                   (-remove
;;                    'null
;;                    (cl-loop
;;                     for osrc in src-blocks collect
;;                     (if (string-match
;;                          (format "<<%s>>" name)
;;                          (org-element-property :value osrc))
;;                         (format "[[%s][%s]]"
;;                                 (org-element-property :name osrc)
;;                                 (org-element-property :name osrc))
;;                       nil)))
;;                   ", ")))
;;            (if (not (string= "" references))
;;                (cons (org-element-property :end src)
;;                      (concat "/Referenced in " references "./\n\n"))
;;              nil)))))
;;     ;; for each block get the name and see if it is in any other block.
;;     (cl-loop for el in (reverse refs) do
;;              (when el
;;                (goto-char (car el))
;;                (insert (cdr el))))))

;; (add-hook 'org-export-before-parsing-hook 'add-references)
