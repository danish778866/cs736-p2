#! /usr/bin/env bash
setYes() {
    # sets kernel config option to yes.
    for option in "$@"
    do
        sed -i "s,^.*CONFIG_${option} .*$,CONFIG_${option}=y," contrib/linux/.config
    done
}

pushd $1
sudo sed -i 's/^# deb-src /deb-src /' /etc/apt/sources.list # enable source repositories
sudo apt-get update -qq
sudo apt-get build-dep -qq linux
sudo apt-get install -qq kernel-package
mkdir contrib
pushd contrib
wget -c https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$KVER.tar.xz
tar xf linux-$KVER.tar.xz
ln -s linux-$KVER linux
cp /boot/config-$(uname -r) linux/.config
popd
# syzkaller basic options
setYes KCOV KCOV_INSTRUMENT_ALL KCOV_ENABLE_COMPARISONS DEBUG_FS \
  DEBUG_INFO KALLSYMS KALLSYMS_ALL CONFIGFS_FS SECURITYFS

# syzkaller bug detection
setYes CONFIG_KASAN CONFIG_KASAN_INLINE

# syzkaller fault injection
setYes FAULT_INJECTION FAULT_INJECTION_DEBUG_FS FAILSLAB \
       FAIL_PAGE_ALLOC FAIL_MAKE_REQUEST FAIL_IO_TIMEOUT FAIL_FUTEX

# syzkaller debugging configs
setYes LOCKDEP PROVE_LOCKING DEBUG_ATOMIC_SLEEP PROVE_RCU DEBUG_VM \
       REFCOUNT_FULL FORTIFY_SOURCE HARDENED_USERCOPY \
       LOCKUP_DETECTOR SOFTLOCKUP_DETECTOR HARDLOCKUP_DETECTOR \
       BOOTPARAM_HARDLOCKUP_PANIC DETECT_HUNG_TASK WQ_WATCHDOG

time make -k -j 32 -C contrib/linux kvmconfig
time make -k -j 32 -C contrib/linux olddefconfig
CONCURRENCY_LEVEL=33 sudo make-kpkg -j32 --initrd --append-to-version=-malos kernel-image kernel-headers
popd
