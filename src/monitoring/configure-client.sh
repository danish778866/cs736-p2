#!/bin/bash

ROOT_DIR=$(cd `dirname $0` && pwd)
COLLECTD_CONF="${ROOT_DIR}/collectd_host.conf"

sudo apt update -qq

sudo apt-get install -qq collectd collectd-utils
sudo cp "${COLLECTD_CONF}" /etc/collectd/collectd.conf
sudo service collectd restart
