#!/bin/bash

ROOT_DIR=$(cd `dirname $0` && pwd)
GRAPHITE_CONF="${ROOT_DIR}/graphite.conf"
CARBON_CONF="${ROOT_DIR}/carbon.conf"
COLLECTD_CONF="${ROOT_DIR}/collectd_server.conf"
GRAFANA_CONF="${ROOT_DIR}/grafana.conf"

sudo apt update -qq

################################################################################
#                                                                              # 
#                       Database setup for monitoring                          #
#                                                                              # 
################################################################################
# Python libraries that Graphite will use to connect and communicate with the database
sudo apt-get -qq install postgresql libpq-dev python-psycopg2

sudo su -c "psql -p 5432 postgres -c \"CREATE USER graphite_user PASSWORD '1234';\"" postgres
sudo su -c "createdb -O graphite_user graphite_db" postgres
sudo su -c "createdb -O graphite_user grafana_db" postgres

################################################################################
#                                                                              #
#                       Graphite setup for monitoring                          #
#                                                                              #
################################################################################
sudo DEBIAN_FRONTEND=noninteractive apt-get install -qq graphite-web graphite-carbon
sudo cp "${GRAPHITE_CONF}" /etc/graphite/local_settings.py
sudo PYTHONPATH=/opt/graphite/webapp graphite-manage migrate --settings=graphite.settings
sudo PYTHONPATH=/opt/graphite/webapp graphite-manage migrate --settings=graphite.settings --run-syncdb
sudo cp /usr/share/doc/graphite-carbon/examples/storage-aggregation.conf.example /etc/carbon/storage-aggregation.conf
# Set CARBON_CACHE_ENABLED=true in /etc/default/graphite-carbon
sudo sed -i 's/CARBON_CACHE_ENABLED=false/CARBON_CACHE_ENABLED=true/g' /etc/default/graphite-carbon
sudo cp "${CARBON_CONF}" /etc/carbon/storage-schemas.conf
sudo rm -f /var/log/graphite/*
sudo service carbon-cache start

################################################################################
#                                                                              #
#                       Apache setup for monitoring                            #
#                                                                              #
################################################################################
sudo apt-get install -qq apache2 libapache2-mod-wsgi
sudo a2dissite 000-default
sudo cp /usr/share/graphite-web/apache2-graphite.conf /etc/apache2/sites-available
sudo a2ensite apache2-graphite
sudo service apache2 reload

# Graphite interface should be accesible at http://IP_ADDRESS

################################################################################
#                                                                              #
#                       Collectd setup for monitoring                          #
#                                                                              #
################################################################################
sudo apt-get install -qq collectd collectd-utils
sudo cp "${COLLECTD_CONF}" /etc/collectd/collectd.conf
sudo service collectd restart

################################################################################
#                                                                              #
#                       Grafana setup for monitoring                           #
#                                                                              #
################################################################################
sudo su -c "echo \"deb https://packagecloud.io/grafana/stable/debian/ stretch main\" >> /etc/apt/sources.list"
sudo curl https://packagecloud.io/gpg.key | sudo apt-key add -
sudo apt-get update -qq
sudo apt-get install grafana -qq
sudo cp "${GRAFANA_CONF}" /etc/grafana/grafana.ini
sudo setcap 'cap_net_bind_service=+ep' /usr/sbin/grafana-server
sudo update-rc.d grafana-server defaults 95 10
sudo service grafana-server start

# Access grafana server at http://<IP_ADDRESS>:3000
