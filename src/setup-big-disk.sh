#! /usr/bin/env bash
# Mount the much larger disk
mkdir ~/virt
sudo mkfs -t ext4 /dev/sda4
sudo mount /dev/sda4 ~/virt
sudo chmod -R a+rwX ~/virt
cat <<EOF | sudo tee -a /etc/fstab
/dev/sda4	/users/`whoami`/virt	ext4    discard,relatime,discard,relatime 0       2
EOF

# preempt root from creating our hosts file.
mkdir -p ~/.ssh
touch ~/.ssh/known_hosts
# don't require strict key checking in our VMs
cat >> ~/.ssh/config <<EOF
Host ubuntu*
    StrictHostKeyChecking no
EOF
