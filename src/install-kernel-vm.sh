#! /usr/bin/env bash
scp ~/virt/contrib/linux-headers-4.19.1-malos_4.19.1-malos-10.00.Custom_amd64.deb ubuntu@$1:~ubuntu/
scp ~/virt/contrib/linux-image-4.19.1-malos_4.19.1-malos-10.00.Custom_amd64.deb ubuntu@$1:~ubuntu/
ssh ubuntu@$1 sudo dpkg -i ~ubuntu/linux-headers-4.19.1-malos_4.19.1-malos-10.00.Custom_amd64.deb
ssh ubuntu@$1 sudo dpkg -i ~ubuntu/linux-image-4.19.1-malos_4.19.1-malos-10.00.Custom_amd64.deb
