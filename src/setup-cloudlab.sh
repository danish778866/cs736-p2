#! /usr/bin/env bash
# installs experimental tools on host + 4 vms

KVER="4.19.1"; export KVER
echo "Expose large file share from /dev/sda4..."
src/setup-big-disk.sh # must happen first.
echo "Building kernel..."
src/build-kernel.sh ~/virt
echo "Installing kvm and build client vms..."
src/install-kvm-vms.sh

echo "Configure clients, resize root partition..."
for i in {1..4}
do
  src/setup-client.sh "ubuntu${i}"
done

# install tools on clients
for i in {1..4}
do
  echo "ubuntu$i: Installing client tools..."
  ssh ubuntu@ubuntu$i cs736-p2/src/install-syzkaller.sh
  
  echo "ubuntu$i: Installing client kernel..."
  src/install-kernel-vm.sh ubuntu$i
  sudo virsh shutdown "ubuntu${i}"
done
sleep 10 # allow shutdowns to finish.

echo "Installing host tools..."
git clone https://github.com/google/syzkaller contrib/syzkaller

echo "Installing host kernel..."
sudo src/install-kernel-host.sh
sudo reboot
